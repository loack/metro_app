require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:michael)
  end
  
  test "layout links" do
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count: 1
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path
    get contact_path
    assert_select "title", full_title("Contact")
  end
  
  test "layout links while logged_in" do
    get root_path
    log_in_as(@user)
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count: 1
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path
    assert_select "a[href=?]", users_path
    assert_select "a[href=?]", user_path(@user)
    assert_select "a[href=?]", edit_user_path(@user)
    assert_select "a[href=?]", '/logout'
    assert_select "a[href=?]", detenteurs_path
    assert_select "a[href=?]", controles_path
    assert_select "a[href=?]", instruments_path
    assert_select "a[href=?]", cartes_instruments_path
    assert_select "a[href=?]", cartes_detenteurs_path
    assert_select "a[href=?]", cartes_controles_path
    assert_select "a[href=?]", reportings_instruments_path
    assert_select "a[href=?]", reportings_taxis_path
    assert_select "a[href=?]", reportings_agents_path
    assert_select "a[href=?]", reportings_ocp_path
    
  end
  
  test "layout links not logged_in" do
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count: 1
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", contact_path
    assert_select "a[href=?]", user_path(@user), count:0
    assert_select "a[href=?]", edit_user_path(@user), count:0
    assert_select "a[href=?]", '/logout', count:0
  end
  

  test "welcome page when not logged_in" do
    get root_path
    assert_template 'static_pages/home'
    assert_select "h1", "Bienvenue sur l'application Metro"
  end
end