require 'test_helper'

class NewControleTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
  end

  test 'should have autocomplete on controle form' do
    log_in_as(@user)
    get new_controle_path
    assert_response :success
    assert_select 'input.form-control'
    #assert_select 'input.autocomplete'
    
  end

end
