require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
  end
  
  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), params: { user: {name: "", email: "invalo@inval",
                                            password: "foo", password_confirmation: "nop"}}
    assert_template 'users/edit'   
    assert_select "div.alert"
  end
  
  test "successful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    name = "Loic" 
    email = "loic@loic.com"
    patch user_path(@user), params: { user: {name: name, email: email,
                                            password: "", password_confirmation: ""}}
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal name, @user.name
    assert_equal email, @user.email
  end
  
  test "successful edit with friendly forwading" do
    get edit_user_path(@user)
    log_in_as(@user)
    assert_redirected_to controles_path
    name = "Loic" 
    email = "loic@loic.com"
    patch user_path(@user), params: { user: {name: name, email: email,
                                            password: "", password_confirmation: ""}}
    assert_not flash.empty?
    assert_redirected_to user_path(@user)
    @user.reload
    assert_equal name, @user.name
    assert_equal email, @user.email
  end
  
  test "friendly forwading only 1 time" do
    get edit_user_path(@user)
    log_in_as(@user)
    assert_redirected_to controles_path
    assert session[:forwading_url].nil?
    get edit_user_path(@user)
    assert_template 'users/edit'
  end
  
end
