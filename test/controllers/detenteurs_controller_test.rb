require 'test_helper'

class DetenteursControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @detenteur = detenteurs(:detenteur_1)
    @user = users(:archer)
  end
  
  test "should get redirected for every action if not logged_in" do
    get new_detenteur_path
    assert_redirected_to login_url
    
    get detenteurs_path
    assert_redirected_to login_url
  
    get detenteur_path(@detenteur)
    assert_redirected_to login_url
    
    get edit_detenteur_path(@detenteur)
    assert_redirected_to login_url
    
    delete detenteur_path(@detenteur)
    assert_redirected_to login_url
  end
  
  test "should get new if logged_in" do
    log_in_as(@user)
    get new_detenteur_path
    assert_response :success
  end

  test "should get index if logged_in" do
    log_in_as(@user)
    get detenteurs_path
    assert_response :success
  end

  test "should get show  if logged_in" do
    log_in_as(@user)
    get detenteur_path(@detenteur)
    assert_response :success
  end

  test "should get edit  if logged_in" do
    log_in_as(@user)
    get edit_detenteur_path(@detenteur)
    assert_response :success
  end

  test "should get destroy if logged_in" do
    log_in_as(@user)
    #redirect to detenteur with alert if not destroyed because of existing controles or instruments
    delete detenteur_path(@detenteur)
    assert_redirected_to detenteur_path(@detenteur)
    follow_redirect!
    assert_select "div.alert"
    
    #redirect to detenteurs list if success
    
    delete detenteur_path(detenteurs(:detenteur_2)) #detenteur sans controles ni instruments
    assert_redirected_to detenteurs_path
    follow_redirect!
    assert_select "div.alert-success"
  
  end

end
