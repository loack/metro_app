require 'test_helper'

class InstrumentsControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @instrument = instruments(:instrument_1)
    @user = users(:archer)
  end
  
  test "should get redirected for every action if not logged_in" do
    get new_instrument_path
    assert_redirected_to login_url
    
    get instruments_path
    assert_redirected_to login_url
  
    get instrument_path(@instrument)
    assert_redirected_to login_url
    
    get edit_instrument_path(@instrument)
    assert_redirected_to login_url
    
    delete instrument_path(instruments(:instrument_1))
    assert_redirected_to login_url
  end  
  
  test "should get new" do
    log_in_as(@user)
    get new_instrument_path
    assert_response :success
  end

  test "should get index" do
    log_in_as(@user)
    get instruments_path
    assert_response :success
    
  end

  test "should get edit" do
    log_in_as(@user)
    get edit_instrument_path(@instrument)
    assert_response :success
  end
  
  test "should get update" do
    log_in_as(@user)
    form_params = {
      instrument: {
      id: 1,
      controle_id: 1,
      categorie_id: 1,
      detenteur_id: 1,
      }
    }
    patch instrument_path(1, form_params)
    assert_redirected_to instruments_url
    follow_redirect!
    assert_not flash.empty?
  end

  test "should get destroy" do
    log_in_as(@user)
    delete instrument_path(@instrument)
    assert_redirected_to instruments_url
    follow_redirect!
    assert_not flash.empty?
  end

end
