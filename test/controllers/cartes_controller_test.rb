require 'test_helper'

class CartesControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:archer)
    log_in_as(@user)
  end
  
  test "should get instruments" do
    get cartes_instruments_url
    assert_response :success
  end

  test "should get controles" do
    get cartes_controles_url
    assert_response :success
  end

  test "should get detenteurs" do
    get cartes_detenteurs_url
    assert_response :success
  end

end
