require 'test_helper'

class SuitesControllerTest < ActionDispatch::IntegrationTest
  test "should get avertissement" do
    get suites_avertissement_url
    assert_response :success
  end

  test "should get pv" do
    get suites_pv_url
    assert_response :success
  end

  test "should get saisie" do
    get suites_saisie_url
    assert_response :success
  end

end
