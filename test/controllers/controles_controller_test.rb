require 'test_helper'

class ControlesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @controle = controles(:controle_1)
    @user = users(:archer)
  end
  
  test "should get redirected for every action if not logged_in" do
    get new_controle_path
    assert_redirected_to login_url
    
    get controles_path
    assert_redirected_to login_url
  
    get controle_path(@controle)
    assert_redirected_to login_url
    
    get edit_controle_path(@controle)
    assert_redirected_to login_url
    
    delete controle_path(controles(:controle_1))
    assert_redirected_to login_url
  end  
  
  test "should get new" do
    log_in_as(@user)
    get new_controle_path
    assert_response :success
  end

  test "should get index" do
    log_in_as(@user)
    get controles_path
    assert_response :success
  end

  test "should get show" do
    log_in_as(@user)
    get controle_path(@controle)
    assert_response :success
  end

  test "should get edit" do
    log_in_as(@user)
    get edit_controle_path(@controle)
    assert_response :success
  end

  test "should get destroy" do
    log_in_as(@user)
    get edit_controle_path(@controle)
    assert_response :success
    delete controle_path(@controle)
    assert_redirected_to controles_path

  end

end
