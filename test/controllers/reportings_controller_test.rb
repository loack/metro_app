require 'test_helper'

class ReportingsControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:archer)
    log_in_as(@user)
  end
  
  test "should get instruments reporting" do
    get reportings_instruments_path
    assert_response :success
  end
  
  test "should get taxis reporting" do
    get reportings_taxis_path
    assert_response :success
  end
  
  test "should get agents reporting" do
    get reportings_agents_path
    assert_response :success
  end
  
  test "should get OCP reporting" do
    get reportings_ocp_path
    assert_response :success
  end
  
end
