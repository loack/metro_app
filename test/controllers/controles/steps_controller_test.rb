require 'test_helper'

class Controles::StepsControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:archer)
    #log_in_as(@user)
  end
  
  test 'should get date step' do
    get new_controle_path
    follow_redirect!
    post :show, {controle_id: 1, id: :date}
    assert_response :success
  end

  test 'should get searchdetenteur' do
    post :show, {id: :searchdetenteur}
    assert_response :success
  end
  
  test 'should get adressecontrole' do
    post :show, {id: :adressecontrole}
    assert_response :success
  end
  
  test 'should get validationnewadresse' do
    post :show, {id: :validationnewadresse}
    assert_response :success
  end

  test 'should get instruments' do
    post :show, {id: :instruments}
    assert_response :success
  end  


end
