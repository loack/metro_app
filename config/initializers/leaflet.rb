Leaflet.tile_layer = 'https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png'
Leaflet.attribution = 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>'
Leaflet.max_zoom = 20