Rails.application.routes.draw do

  #cartes
  get 'cartes/instruments'
  post 'cartes/choice'
  get 'cartes/controles'
  get 'cartes/detenteurs'
  
  #reporting
  get 'reportings/instruments'
  get 'reportings/taxis'
  get 'reportings/agents'
  get 'reportings/ocp'

  root 'static_pages#home'
  get  '/help',    to: 'static_pages#help'
  get  '/about',   to: 'static_pages#about'
  get  '/contact', to: 'static_pages#contact'
  get  '/chat',    to: 'static_pages#chat'
  
  #user controller
  get  '/signup',  to: 'users#new'
  post '/signup',  to: 'users#create'
  
  #login controller
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  resources :users
  
  resources :account_activations, only: [:edit]

  resources :password_resets,     only: [:new, :create, :edit, :update]
 
  resources :controles do 
    #route pour l'autocomplete d'adresse
    get :autocomplete_adress_label, :on => :collection
    resources :steps, only: [:show, :update], controller: 'controle/steps'
  end
  
  resources :detenteurs
  resources :instruments
  resources :adresses
  resources :create_controle
  
  get 'adresses/new'
  
  post 'adresses/validate'
  
  post 'adresses/search'

  post 'adresses/create'

  get 'adresses/edit'

  get 'adresses/update'
  
  get 'detenteurs/new'

  get 'detenteurs/index'

  get 'detenteurs/show'

  get 'detenteurs/edit'
  
  post 'detenteurs/validate'
  
  post 'detenteurs/search'
  
  post 'detenteurs/select_adress'
  
  get 'detenteurs/destroy'



  get 'suites/avertissement'

  get 'suites/pv'

  get 'suites/saisie'

  
end
