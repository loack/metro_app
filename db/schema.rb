# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190902190028) do

  create_table "adresses", force: :cascade do |t|
    t.string "label"
    t.string "housenumber"
    t.string "postcode"
    t.string "citycode"
    t.string "city"
    t.float "lat"
    t.float "long"
    t.string "context"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "departement_id"
    t.index ["departement_id"], name: "index_adresses_on_departement_id"
    t.index ["label"], name: "index_adresses_on_label", unique: true
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.string "catid"
    t.index ["name"], name: "index_categories_on_name", unique: true
  end

  create_table "controles", force: :cascade do |t|
    t.string "date"
    t.integer "user_id"
    t.string "adress_label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "detenteur_id"
    t.integer "adress_id"
    t.index ["adress_id"], name: "index_controles_on_adress_id"
    t.index ["detenteur_id"], name: "index_controles_on_detenteur_id"
    t.index ["user_id"], name: "index_controles_on_user_id"
  end

  create_table "departements", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "region_id"
    t.index ["region_id"], name: "index_departements_on_region_id"
  end

  create_table "detenteurs", force: :cascade do |t|
    t.string "name"
    t.string "societe"
    t.string "siren"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "adress_id"
    t.index ["adress_id"], name: "index_detenteurs_on_adress_id"
    t.index ["name"], name: "index_detenteurs_on_name", unique: true
  end

  create_table "instruments", force: :cascade do |t|
    t.integer "controle_id"
    t.text "details"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "categorie_id"
    t.integer "detenteur_id"
    t.string "dernierevp"
    t.boolean "carnetmetro"
    t.boolean "accepte"
    t.boolean "avertissement"
    t.boolean "scelles"
    t.text "motif"
    t.boolean "saisie"
    t.boolean "pva"
    t.boolean "pvp"
    t.boolean "refuse"
    t.index ["categorie_id"], name: "index_instruments_on_categorie_id"
    t.index ["controle_id"], name: "index_instruments_on_controle_id"
    t.index ["detenteur_id"], name: "index_instruments_on_detenteur_id"
  end

  create_table "regions", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "suites", force: :cascade do |t|
    t.integer "controle_id"
    t.string "type"
    t.string "dateenvoi"
    t.string "deadline"
    t.string "sanction"
    t.string "montant"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["controle_id"], name: "index_suites_on_controle_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "password_digest"
    t.string "remember_digest"
    t.boolean "admin", default: false
    t.string "activation_digest"
    t.boolean "activated", default: false
    t.datetime "activated_at"
    t.string "reset_digest"
    t.datetime "reset_sent_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "region"
    t.string "profile"
    t.integer "region_id"
    t.integer "departements_ids"
    t.index ["email"], name: "index_users_on_email", unique: true
  end

end
