class AddAdressToControle < ActiveRecord::Migration[5.1]
  def change
    add_reference :controles, :adress, foreign_key: true
  end
end
