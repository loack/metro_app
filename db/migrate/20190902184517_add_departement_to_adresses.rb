class AddDepartementToAdresses < ActiveRecord::Migration[5.1]
  def change
    add_reference :adresses, :departement, foreign_key: true
  end
end
