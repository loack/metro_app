class AddDetenteurToInstruments < ActiveRecord::Migration[5.1]
  def change
    add_reference :instruments, :detenteur, foreign_key: true
  end
end
