class CreateControles < ActiveRecord::Migration[5.1]
  def change
    create_table :controles do |t|
      t.string :date
      t.references :user, foreign_key: true
      t.string :adress_label

      t.timestamps
    end
  end
end
