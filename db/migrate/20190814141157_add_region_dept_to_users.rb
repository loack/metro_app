class AddRegionDeptToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :region_id, :integer
    add_column :users, :departements_ids, :integer, array:true
  end
end
