class AddPvsToInstruments < ActiveRecord::Migration[5.1]
  def change
    add_column :instruments, :pva, :boolean
    add_column :instruments, :pvp, :boolean
  end
end
