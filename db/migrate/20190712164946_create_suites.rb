class CreateSuites < ActiveRecord::Migration[5.1]
  def change
    create_table :suites do |t|
      t.references :controle, foreign_key: true
      t.string :type
      t.string :dateenvoi
      t.string :deadline
      t.string :sanction
      t.string :montant
      t.string :status
      
      t.timestamps
    end

  end
end
