class RemoveRegionToDepartements < ActiveRecord::Migration[5.1]
  def change
    remove_column :departements, :region_id
  end
end
