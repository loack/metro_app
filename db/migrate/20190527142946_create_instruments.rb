class CreateInstruments < ActiveRecord::Migration[5.1]
  def change
    create_table :instruments do |t|
      t.references :controle, foreign_key: true
      t.text :details
      t.timestamps
    end
  end
end
