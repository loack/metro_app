class AddSaisieToInstruments < ActiveRecord::Migration[5.1]
  def change
    add_column :instruments, :saisie, :boolean
  end
end
