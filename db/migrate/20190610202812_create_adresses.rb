class CreateAdresses < ActiveRecord::Migration[5.1]
  def change
    create_table :adresses do |t|
      t.string :label
      t.string :housenumber
      t.string :postcode
      t.string :citycode
      t.string :city
      t.float :lat
      t.float :long
      t.string :context
      t.timestamps
    end
    add_index :adresses, :label, unique: true
  end
end
