class AddRegionToDepartements < ActiveRecord::Migration[5.1]
  def change
    add_reference :departements, :region, foreign_key: true
  end
end
