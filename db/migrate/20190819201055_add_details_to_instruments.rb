class AddDetailsToInstruments < ActiveRecord::Migration[5.1]
  def change
    add_column :instruments, :dernierevp, :string
    add_column :instruments, :carnetmetro, :boolean
    add_column :instruments, :accepte, :boolean
    add_column :instruments, :avertissement, :boolean
    add_column :instruments, :scelles, :boolean
    add_column :instruments, :motif, :text
  end
end
