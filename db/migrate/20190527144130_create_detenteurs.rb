class CreateDetenteurs < ActiveRecord::Migration[5.1]
  def change
    create_table :detenteurs do |t|
      t.string :name
      t.string :societe
      t.string :siren

      t.timestamps
    end
    add_index :detenteurs, :name, unique: true
  end
end
