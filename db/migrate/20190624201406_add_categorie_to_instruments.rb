class AddCategorieToInstruments < ActiveRecord::Migration[5.1]
  def change
    add_reference :instruments, :categorie, foreign_key: true
  end
end
