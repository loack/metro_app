class CreateDepartements < ActiveRecord::Migration[5.1]
  def change
    create_table :departements do |t|
      t.string :name
      t.integer :region_id
      t.timestamps
    end
  end
end
