class AddAdressToDetenteur < ActiveRecord::Migration[5.1]
  def change
    add_reference :detenteurs, :adress, foreign_key: true
  end
end
