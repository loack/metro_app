$(document).on 'turbolinks:load', ->
  if (!$.fn.DataTable.isDataTable('#instruments-datatable'))
    $('#instruments-datatable').dataTable
      processing: true
      serverSide: true
      responsive: true
      ajax:
        url: $('#instruments-datatable').data('source')
      pagingType: 'full_numbers'
      columns: [
        {data: 'categorie'}
        {data: 'detenteur'}
        {data: 'datecontrole'}
        {data: 'adresscontrole'} 
        {data: 'details'} 
        {data: 'links'}]
      buttons:[ 
        { extend: 'copy', text: 'Copier', className: 'btn-primary' }
        { extend: 'csv', text: 'Export CSV', className: 'btn-primary' }
        ]
      dom:  "<'row'<'col'l><'col' B><'col'f>>" + "<'row'<'col'tr>>" +"<'row'<'col'i><'col'p>>"