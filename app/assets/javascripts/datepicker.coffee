#Config du datepicker pour les controles (step controller)
$(document).on 'turbolinks:load', ->
    $('#controle_date').datepicker
        format: "dd/mm/yy"
        autoclose: true
        todayHighlight: true