# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'turbolinks:load', ->
  if (!$.fn.DataTable.isDataTable('#controles-datatable'))
    $('#controles-datatable').dataTable
      processing: true
      serverSide: true
      responsive: true
      ajax:
        url: $('#controles-datatable').data('source')
      pagingType: 'full_numbers'
      columns: [
        {data: 'date'}
        {data: 'detenteur'}
        {data: 'adress'}
        {data: 'user'} 
        {data: 'instruments'} ]
      buttons:[ 
        { extend: 'copy', text: 'Copier', className: 'btn-primary' }
        { extend: 'csv', text: 'Export CSV', className: 'btn-primary' }
        ]
      dom:  "<'row'<'col'l><'col' B><'col'f>>" + "<'row'<'col'tr>>" +"<'row'<'col'i><'col'p>>"
      # pagingType is optional, if you want full pagination controls.
      # Check dataTables documentation to learn more about
      # available options.
