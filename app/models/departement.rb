class Departement < ApplicationRecord
  belongs_to :region
  has_many :adresses
end
