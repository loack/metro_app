class Region < ApplicationRecord
  has_many :departements
  has_many :adresses, :through => :departements
end
