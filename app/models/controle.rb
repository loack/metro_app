class Controle < ApplicationRecord
  belongs_to :user
  belongs_to :detenteur, optional: true

  
  #pour permettre le formulaire imbriqué d'instruments dans les contrôles
  has_many :instruments, inverse_of: :controle
  accepts_nested_attributes_for :instruments, reject_if: :all_blank, allow_destroy: true
  
  
  #validation du contrôle step by step
  validates :date, :user, presence: true,
		  if: -> { required_for_step?(:date) }
		    
  #on definit les étapes de validation
  cattr_accessor :form_steps do
  	%w(date searchdetenteur detenteur adressecontrole validationnewadresse instruments resume)
  end
  #curseur d'étape
  attr_accessor :form_step
  
  def required_for_step?(step)
  # All fields are required if no form step is present
  return true if form_step.nil?
  # All fields from previous steps are required if the
  # step parameter appears before or we are on the current step
  return true if self.form_steps.index(step.to_s) <= self.form_steps.index(form_step)
  end

end
