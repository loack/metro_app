class Adress < ApplicationRecord
  validates :label, presence: true
  validates :city, presence: true
  validates :citycode, presence: true
  
  belongs_to :departement
  delegate :region, :to => :departement
  
  def region
    Region.where(name: self.context)
  end
  
  def codepostal_to_departement
    if self.postcode[0..1] == "97"
      #département outre-mer
      return self.postcode[0..2].to_i
    else 
      #département métropolitain
      return self.postcode[0..1].to_i
    end
  end
end

