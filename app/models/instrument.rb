class Instrument < ApplicationRecord
  belongs_to :controle
  belongs_to :detenteur
  belongs_to :categorie
  
  serialize :details, Hash
end
