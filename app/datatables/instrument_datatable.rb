class InstrumentDatatable < AjaxDatatablesRails::ActiveRecord
  extend Forwardable
  def_delegators :@view, :link_to, :edit_instrument_path, :instrument_path, :listview
  
  def initialize(params, opts = {})
    @view = opts[:view_context]
    super
  end
  
  
  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      categorie: {source: "Instrument.categorie_id", cond: :like, searchable: true, orderable: true},
      detenteur: {source: "Instrument.detenteur_id", cond: :like, searchable: true, orderable: true},
      details: {source: "Instrument.details", cond: :like, searchable: true, orderable: false},
      datecontrole: {source: "Instrument.controle", cond: :like, searchable: true, orderable: true},
      adresscontrole: {source: "Instrument.controle", cond: :like, searchable: true, orderable: false},
      links: {source: "Instrument"}
    }
  end

  def data
    records.map do |record|
      {
        categorie: record.categorie.name,
        detenteur: record.detenteur.name,
        details: listview(record.details),
        datecontrole: getattribute(record.controle, "date"),
        adresscontrole: adresselabel(record),
        links: link_to('Editer', edit_instrument_path(record))
      }
    end
  end
  def adresselabel(record)
    if !!record.controle
      if !!record.controle.adress_id
        if !Adress.find_by_id(record.controle.adress_id)
          "pas d'adresse définie"
        else
          Adress.find_by_id(record.controle.adress_id).label
        end
      else
        "pas d'adresse définie"
      end
    else
      "pas de contrôle sur cet instrument"
    end
  end
  
  def getattribute(record, attribute)
    if record.nil?
      return "non défini"
    else
      if !!record.attributes[attribute]
        return record.attributes[attribute]
      else
        return "non défini"
      end
    end
  end
  
  def get_raw_records
    Instrument.all
  end
end
