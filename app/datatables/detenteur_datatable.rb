class DetenteurDatatable < AjaxDatatablesRails::ActiveRecord

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
       name: { source: "Detenteur.name", cond: :like, searchable: true, orderable: true },
       societe: {source: "Detenteur.societe", cond: :like, searchable: true, orderable: true },
       siren: {source: "Detenteur.siren", cond: :eq, searchable: true },
       adress_id: {source: "Detenteur.adress_id" }
       }
  end

  def data
    records.map do |record|
      {
        name: getattribute(record, "name"),
        societe: getattribute(record,"societe"),
        siren: getattribute(record,"siren"),
        adresse: adresselabel(record)
      }
    end
  end
  
  def getattribute(record, attribute)
    if record.nil?
      return "non défini"
    else
      if !!record.attributes[attribute]
        return record.attributes[attribute]
      else
        return "non défini"
      end
    end
  end
  
  def adresselabel(record)
    if !!record.adress_id
      if !Adress.find_by_id(record.adress_id)
        "pas d'adresse définie"
      else
        Adress.find_by_id(record.adress_id).label
      end
    else
      "pas d'adresse définie"
    end
  end
  

  def get_raw_records
    # insert query here
    if !!options[:region]
    #adresse dans la région donnée
    Detenteur.where(adress_id: Adress.where(departement: options[:region].departements).ids)
    else
    #sinon toute la France
      Detenteur.all
    end
    
  end

end
