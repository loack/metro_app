class ControleDatatable < AjaxDatatablesRails::ActiveRecord
  extend Forwardable
  def_delegators :@view, :link_to, :edit_controle_path, :controle_path
  
  def initialize(params, opts = {})
    @view = opts[:view_context]
    super
  end
  
  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      date: {source: "Controle.date", cond: :like, searchable: true, orderable: true},
      detenteur: {source: "Controle.detenteur", cond: :like, searchable: true, orderable: true},
      adress: {source: "Controle.adress_id", cond: :like, searchable: true, orderable: true},
      user: {source: "Controle.user", cond: :like, searchable: true, orderable: true},
      instruments: {source: "Controle.instruments", cond: :like, searchable: true, orderable: true}
    }
  end

  def data
    records.map do |record|
      {
        date: link_to(getattribute(record, "date"), edit_controle_path(record)),
        detenteur: getattribute(record.detenteur, "name"),
        adress: adresselabel(record),
        user: getattribute(record.user,"name"),
        instruments: nombre_instruments(record),
      }
    end
  end
  
  def getattribute(record, attribute)
    if record.nil?
      return "non défini"
    else
      if !!record.attributes[attribute]
        return record.attributes[attribute]
      else
        return "non défini"
      end
    end
  end
  def adresselabel(record)
    if !!record.adress_id
      if !Adress.find_by_id(record.adress_id)
        "pas d'adresse définie"
      else
        Adress.find_by_id(record.adress_id).label
      end
    else
      "pas d'adresse définie"
    end
  end
  

  def get_raw_records
    # insert query here
    Controle.all
  end

    
  def nombre_instruments(controle)
    str = ""
    if controle.instruments.count > 0
      controle.instruments.group(:categorie).count.to_a.each do |cat|
          str += "#{cat[1]} #{cat[0].name}, "
       end
    else
      str = "Aucun instrument contrôlé"
    end
    str
  end
  
end
