module AdressesHelper
  
  def search_adresse(requete)
    # on prépare la requète qui va servir pour la recherche sur l'api adresses.
    req = requete.gsub(" ","+")
    req = "https://api-adresse.data.gouv.fr/search/?q="+req+"&limit=15"
   
    results = JSON.parse(HTTP.get(req).to_s)
    
    #on traite le résultat
    if results["features"].length == 0
      return false 
      
    else
      return results["features"]
    end
    
  end
  
  def get_adresses_from_results(results)
      #créer la collection d'adresses trouvées et on la met en forme pour le collection_select
      adresses = results.map { |r| [r["properties"]["label"], {
                      housenumber:r["properties"]["housenumber"],
                      label:r["properties"]["label"],
                      postcode:r["properties"]["postcode"],
                      citycode:r["properties"]["citycode"],
                      city:r["properties"]["city"],
                      context:r["properties"]["context"],
                      lat:r["geometry"]["coordinates"][1],
                      long:r["geometry"]["coordinates"][0],
                      departement_id:code_postal_to_departement(r["properties"]["postcode"]),
                      } ]
          }
      return adresses
  end
  
  def get_markers_from_results(results)
      #les markers sur la carte
      markers = results.map { |r| {:latlng => [r["geometry"]["coordinates"][1],r["geometry"]["coordinates"][0]], :popup => r["properties"]["label"]}}
      return markers
  end
  
  #markers pour la carte avec api insee
  def extract_api_markers(results)
    if !results["etablissement"].nil?
      if !results["total_results"]
        e = results["etablissement"]
        return [{:latlng => [e["latitude"].to_f, e["longitude"].to_f], :popup => e["nom_raison_sociale"]}]
      else
        return results["etablissement"].map{|e| {:latlng => [e["latitude"].to_f, e["longitude"].to_f], :popup => e["nom_raison_sociale"]} }
      end
    else 
        return false
    end
  end

  def generate_bounds_from_markers(markers)  
    xs = markers.map { |m| m[:latlng][0] }
    ys = markers.map { |m| m[:latlng][1] }
    space = 0.01
    bounds = [[xs.min-space,ys.min-space],[xs.max+space,ys.max+space,]]
    return bounds
  end
  
  
  def create_adresse_if_new(val)
    if Adress.find_by(label: val[:label]).nil?
      if adresse = Adress.create!(val)
        flash[:success] = "L'adresse, #{val[:label]}, est créée "
        return adresse
      else
        flash[:danger] = "L'adresse n'a pas pu être créée"
      end
    else
      flash[:info] = "Adresse déjà présente dans la base de données"
      return Adress.find_by(label: val[:label])
    end
  end
  
  #conversion code postal en département
  def code_postal_to_departement(codepostal)
    if codepostal[0..1] == "97"
      puts "département outre-mer"
      return codepostal[0..2].to_i
    else
      puts "département métropolitain"
      return codepostal[0..1].to_i
    end
  end
  
end
