module DetenteursHelper
  def adress_for(detenteur)
    if Adress.where(:id => detenteur.adress_id).exists?
      @adress = Adress.find(detenteur.adress_id)
    else
      return false
    end
  end
end
