module InstrumentsHelper
  def listview(details)
    list = details.map{ |k,v| "<strong>#{k} : </strong>#{v}"}
    html = ""
    list.each do |e|
      html += e + "&emsp;"
    end
    html.html_safe
  end
end
