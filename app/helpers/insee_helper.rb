module InseeHelper
  
  # Gestion de la connection à l'API INSEE
  #récupérer un nouveau token
  def get_api_token
    require 'net/http'
    require 'uri'
    require 'openssl'
    uri = URI.parse("https://api.insee.fr/token")
    request = Net::HTTP::Post.new(uri)
    request["Authorization"] = Authorization_sirene
    request.set_form_data("grant_type" => "client_credentials",)
    req_options = {use_ssl: uri.scheme == "https",verify_mode: OpenSSL::SSL::VERIFY_NONE,}

    response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      http.request(request)
    end
    json_response = JSON.parse(response.body)
    token = json_response["access_token"]
    validity = Time.now + json_response["expires_in"]
    return {:token => token,:validity =>  validity}
    
  end
  #vérifier si le token est encore valide
  def check_api_token(token_data)
    if token_data[:validity] > Time.now + 60
      return "Token #{token_data[:token]} encore valable jusqu'au #{token_data[:validity]}"
    else
      $Token_sirene = get_api_token
      return "Token #{$Token_sirene[:token]} mis à jour et valable jusqu'au #{$Token_sirene[:validity]}"
    end
  end
  

  
  #recherche sur l'api entreprise.data.gouv.fr
  def search_detenteur(requete)
    if !(requete =~ /^[0-9]{14}$/).nil? 
      search_siret(requete)
    else
      if params[:page].nil? then pagenum = 1 else pagenum = params[:page] end
        
      if requete.include?("?")
        options = requete[requete.index("?")+1,requete.length] 
        requete = requete[0,requete.index("?")-1]
      else 
        options="" 
      end
      requete = requete + "?page=#{pagenum}&per_page=10" + "&" + options
      requete = "https://entreprise.data.gouv.fr/api/sirene/v1/full_text/"+requete
      res = HTTP.get(requete)
      res = JSON.parse(res)
      if res["message"].nil?
        return res
      else
        return false
      end
    end
  end
  
  def search_siret(siret)
    requete = "https://entreprise.data.gouv.fr/api/sirene/v1/siret/"+siret.to_s
    return JSON.parse(HTTP.get(requete))
  end
  
  
  #extraction des resultats du JSON obtenu de l'API
  def extract_data(results)
    etab = results["etablissement"]
      if !etab.nil?
        if !results["total_results"]
          return [["&emsp;<b>#{etab["nom_raison_sociale"]}</b> - <i>#{etab["siret"]}</i> <p>&emsp;#{etab["l2_normalisee"]} #{etab["l3_normalisee"]} 
          #{etab["l4_normalisee"]} #{etab["l5_normalisee"]} #{etab["l6_normalisee"]} #{etab["l7_normalisee"]}</p>".html_safe,
          etab["siret"]]]
        else
          return etab.map{|e| ["&emsp;<b>#{e["nom_raison_sociale"]}</b> - <i>#{e["siret"]}</i> <p>&emsp;#{e["l2_normalisee"]} #{e["l3_normalisee"]} 
            #{e["l4_normalisee"]} #{e["l5_normalisee"]} #{e["l6_normalisee"]} #{e["l7_normalisee"]}</p>".html_safe,
            e["siret"]]}
        end
      else 
        return false
      end
  end

end