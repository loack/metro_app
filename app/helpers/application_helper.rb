module ApplicationHelper
 
  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = "Base de donnée Métrologie"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end
  
  
  # Return button name function of controller action
  def button_name
    action = params[:action]
    controller = params[:controller]
    case action
    when 'new'
      return "Ajouter #{controller.singularize}"  
    when 'edit'
      return "Modifier #{controller.singularize}"
    when 'create'
      return "Ajouter #{controller.singularize}"  
    end
  end  
  

end
