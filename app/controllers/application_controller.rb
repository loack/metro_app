class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  
  #ajout du module SessionHelper dans toutes les pages de l'appli
  include SessionsHelper

  #module pour la connection à l'API INSEE
  include InseeHelper
  
  #module pour la récupération des adresses
  include AdressesHelper
  

  
  
end
