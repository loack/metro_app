class DetenteursController < ApplicationController
  #user must be logged-in to see detenteurs and operate
  before_action :logged_in_user
  
  def new
    @detenteur = Detenteur.new()
  end

  def validate
    @res = eval(params[:etablissement][:valeur])
    @ad = @res["adresseEtablissement"]
    params[:adress] = @res["adresseEtablissement"]
  end
  
  def create
    det = params.require(:detenteur).permit(:name, :societe, :siren)
    ad = params.require(:detenteur).permit(:housenumber, :label, :city, :citycode, :postcode)
    
    #si l'adresse n'existe pas on la crée
    if Adress.where(label:ad[:label]).exists?
      @adress = Adress.find_by(label:ad[:label])
    else
      @adress = Adress.create(ad)
    end
    # si le detenteur n'existe pas on le crée
    if Detenteur.where(name: det[:name]).exists?
      @detenteur = Detenteur.find_by(name: det[:name])
    else
      @detenteur = Detenteur.create(det)
    end
    @detenteur.update_attribute(:adress_id, @adress.id)
    redirect_to edit_adress_path(@adress)
  end

  def index
    @region = params[:region] #définir ici le paramètre de la région voulue
    respond_to do |format|
      format.html
      format.json { render json: DetenteurDatatable.new(params, region: @region ) } #affichage de la datatable avec des options 
    end
  end

  def show
    @detenteur = Detenteur.find(params[:id])
    @instruments = Instrument.where(detenteur_id:@detenteur.id)
  end
  
  def update
  end

  def edit
    @detenteur = Detenteur.find(params[:id])
    
  end

  def destroy
   @detenteur = Detenteur.find(params[:id])
    if @detenteur.instruments.exists? | @detenteur.controles.exists?
      flash[:danger] = "Le détenteur est lié à des instruments ou des contrôles"
      redirect_to detenteur_path(@detenteur)
    else
      @detenteur.destroy
      flash[:success] = "Detenteur supprimé"
      redirect_to detenteurs_url
    end
  end


end
 