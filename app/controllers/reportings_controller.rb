class ReportingsController < ApplicationController
  before_action :logged_in_user
  
  def agents
  end
  
  def instruments
    @data = []
    Categorie.all.each do |cat|
      line = [
        cat.name, 
        cat.instruments.count, 
        Instrument.group(:controle_id,:categorie_id).having("categorie_id= ?", cat.id).count.values.sum,
        cat.instruments.where(accepte:false).count,
        cat.instruments.where(avertissement:true).count,
        cat.instruments.where(pvp:true).count,
        cat.instruments.where(pva:true).count,
        cat.instruments.where(saisie:true).count,
        0
        ]
      @data.append(line)
    end
  end
  
  def taxis
  end
  
  def ocp 
  end
end
