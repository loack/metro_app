class CartesController < ApplicationController
  before_action :logged_in_user
  
  def instruments
    @markers = generate_markersInstruments(Instrument.all)
    @bounds = generate_bounds(@markers)
  end

  def controles
    @markers = generate_markers(Controle.all)
    @bounds = generate_bounds(@markers)
  end

  def detenteurs
    @markers = generate_markers(Detenteur.all)
    @bounds = generate_bounds(@markers)
  end
  
  def choice
    instruments = Instrument.where(categorie_id: params[:request][:categorie_id])
    @markers = generate_markersInstruments(instruments)
    @bounds = generate_bounds(@markers)
    render 'instruments'
  end


  protected
  def generate_markers(objectlist)
    markers = []
    objectlist.each do |c|
    if !!c.adress_id
      lat = Adress.find(c.adress_id).lat
      long = Adress.find(c.adress_id).long
      if !!lat & !!long
        mark = {latlng:[lat,long]}
        markers.append(mark)
      end
    end
    end
    return markers
  end
  
  def generate_bounds(markers)  
    xs = markers.map { |m| m[:latlng][0] }
    ys = markers.map { |m| m[:latlng][1] }
    bounds = [[xs.min-2,ys.min-2],[xs.max+2,ys.max+2,]]
  return bounds
  end

  def generate_markersInstruments(objectlist)
    markers = []
    objectlist.each do |c|
    if !!c.controle && !!c.controle.adress_id
      lat = Adress.find(c.controle.adress_id).lat
      long = Adress.find(c.controle.adress_id).long
      if !!lat & !!long
        mark = {latlng:[lat,long]}
        markers.append(mark)
      end
    end
    end
    return markers
  end
end