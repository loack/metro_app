class InstrumentsController < ApplicationController
  #user must be logged-in to see instruments and operate
  before_action :logged_in_user
  
  def new
    @instrument = Instrument.new()
  end

  def index
    respond_to do |format|
      format.html
      format.json { render json: InstrumentDatatable.new(params, view_context: view_context) }
    end
  end

  def edit
    @instrument = Instrument.find(params[:id])
  end

  def update
    @instrument = Instrument.find(params[:id])
    if @instrument.update_attributes(instrument_params)
      flash[:success] = "Instrument modifié"
      redirect_to instruments_path
    else
      render 'edit'
    end
  end
  def destroy
    Instrument.find(params[:id]).destroy
    flash[:success] = "Instrument supprimé"
    redirect_to instruments_url
  end
  
  def instrument_params
    params.require(:instrument)
    .permit(:name, :detenteur_id, :categorie_id, :_destroy, :dernierevp, :carnetmetro, :refuse, :avertissement, :pv, :scelles, :motif,
      details:  [:marque, :type, :numserie])
  end
end
