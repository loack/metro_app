class UsersController < ApplicationController
 #verifier que l'user est loggé et que c'est le bon avant de l'autoriser à edit ou update
 before_action :logged_in_user, only: [:edit, :update, :index, :destroy]
 before_action :correct_user,   only: [:edit, :update]
 before_action :admin_user,     only: :destroy

  def new
    @user = User.new
  end
  
  def index
    @users = User.where(activated: true).paginate(page: params[:page])
  end
  
  def show
    @user = User.find(params[:id])
    redirect_to root_url and flash[:info] = "User not activated" unless @user.activated?
    
  end
  
  def create
    @user = User.new(user_params)
    if @user.save #si tout est ok on save l'user et on lui envoie un mail d'activation
      @user.activate
      #@user.send_activation_email
      #flash[:info] = "Please check your email to activate your account."
      flash[:info] = "Utilisateur créé et activé"
      @user.remember
      redirect_to root_url
      
    else #sinon on le renvoie sur la page de création (avec affichage des erreurs géré por les validations)
      render 'new'
    end
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "User profile is updated"
      redirect_to @user
    else
      render 'edit'
    end
  end
  
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end

  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless current_user?(@user)
  end
  
  # Confirms an admin user.
  def admin_user
    redirect_to(root_url) unless current_user.admin?
  end
  
  private

    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end
    
    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        flash[:danger] = "Veuillez vous connecter pour accèder à cette page."
        redirect_to login_url
      end
    end  
    
    
end
