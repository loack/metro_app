class AdressesController < ApplicationController

  def new
  end
  
  def search
    # on prépare la requète qui va servir pour la recherche sur l'api adresses.
    req = params[:adress][:label] + " " + params[:adress][:postcode] + ' ' + params[:adress][:city]
    req = req.gsub(" ", "+")
    req = "https://api-adresse.data.gouv.fr/search/?q="+req+"&limit=15"
    @result = JSON.parse(HTTP.get(req).to_s)
    
    #on traite le résultat
    if @result["features"].length == 0
      flash.now[:danger] = "Pas de résultat" 
      render :action => 'new'
    else
      nb = @result["features"].length
      flash.now[:success] = nb.to_s + " résultat".pluralize(nb)+" trouvés".pluralize(nb)
      @result = @result["features"]
      
      #créer la collection d'adresses trouvées et on la met en forme pour le collection_select
      @adresses = @result.map { |r| [r["properties"]["label"], {
                      housenumber:r["properties"]["housenumber"],
                      label:r["properties"]["label"],
                      postcode:r["properties"]["postcode"],
                      citycode:r["properties"]["citycode"],
                      city:r["properties"]["city"],
                      context:r["properties"]["context"],
                      lat:r["geometry"]["coordinates"][1],
                      long:r["geometry"]["coordinates"][0],
                      } ]
          }
      #ainsi que les markers sur la carte
      @markers = @result.map { |r| {:latlng => [r["geometry"]["coordinates"][1],r["geometry"]["coordinates"][0]], :popup => r["properties"]["label"]}}
      #on calcule des bords de la carte pour l'afficher proprement
      @bounds = generate_bounds_from_markers(@markers)

    #on affiche les résultats sur la page de création
      render :action => 'new'
    end
  end
  
  def create
    val = eval(params.require(:adress).permit(:attributes)[:attributes])
    puts "resultat"
    puts val
    if Adress.find_by(label:val[:label]).nil?
      if Adress.create!(val)
        puts [val[:lat],val[:long]]
        flash[:success] = "L'adresse, #{val[:label]}, est créée "
      else
        flash[:danger] = "L'adresse n'a pas pu être créée"
      end
    else
      flash[:info] = "Adresse déjà présente dans la base de données"
    end
    redirect_to new_adress_path
  end

  def show
    @adress = Adress.find(params[:id])
  end
  
  def edit
    @adress = Adress.find(params[:id])
    #enrichir l'adresse de la géoloc si non disponible
    if !(!!@adress.lat|!!@adress.long)
      @rich = false
      @adresses = recherche_adresse(@adress.label)
    else
      @rich = true
    end
  end

  def update
    @adress = Adress.find(params[:id])
    if params[:adress][:attributes].nil?
      par = params.require(:adress).permit(:label, :housenumber, :city, :citycode, :postcode, :lat, :long, :context)
    else
      par = eval(params.require(:adress).permit(:attributes)[:attributes])
    end
      if @adress.update_attributes(par)
        puts "les paramètres :"
        puts par
        flash[:success] = "Adresse modifiée"
        redirect_to @adress
      else
        render 'edit'
      end
  end
  
  def destroy
    Adress.find(params[:id]).destroy
    flash[:success] = "Adresse supprimée"
    redirect_to adresses_url
  end
  
  def recherche_adresse(requete)
    # on prépare la requète qui va servir pour la recherche sur l'api adresses.
    req = "https://api-adresse.data.gouv.fr/search/?q="+requete.gsub(" ", "+")+"&limit=15"
    @result = JSON.parse(HTTP.get(req).to_s)
    
    #on traite le résultat
    if @result["features"].length == 0
      return []
    else
      @result = @result["features"]
      #créer la collection d'adresses trouvées et on la met en forme pour le collection_select
      @adresses = @result.map { |r| [r["properties"]["label"], {
                      housenumber:r["properties"]["housenumber"],
                      label:r["properties"]["label"],
                      postcode:r["properties"]["postcode"],
                      citycode:r["properties"]["citycode"],
                      city:r["properties"]["city"],
                      context:r["properties"]["context"],
                      lat:r["geometry"]["coordinates"][1],
                      long:r["geometry"]["coordinates"][0],
                      } ]
          }
      #ainsi que les markers sur la carte
      @markers = @result.map { |r| {:latlng => [r["geometry"]["coordinates"][1],r["geometry"]["coordinates"][0]], :popup => r["properties"]["label"]}}
      #on calcule des bords de la carte pour l'afficher proprement
      @bounds = generate_bounds(@markers)
      return @adresses
    end
  end
end

