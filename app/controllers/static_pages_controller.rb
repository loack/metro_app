class StaticPagesController < ApplicationController
  def home
      @controles = Controle.all if logged_in?
      @controle = current_user.controles.build if logged_in?
  end
  
  def chat
      if logged_in?
        @micropost  = current_user.microposts.build
        #@feed_items = current_user.feed.paginate(page: params[:page])
        @feed_items = Micropost.paginate(page: params[:page])
      end
  end

  def help
    
  end
  
  def about
    
  end
  
  def contact
    
  end
  
  def testpage
    
  end
end
