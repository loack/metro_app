class Controle::StepsController < ApplicationController
  #before_action :logged_in_user
  include Wicked::Wizard
  
  steps *Controle.form_steps
  
  def show
    @controle = Controle.find(params[:controle_id])
    case step
    when "date"
    when "searchdetenteur"
      @form = DetenteurForm.new()
    when "detenteur"
      @results = search_detenteur(session[:requete])
      if !@results
        flash[:danger] = "Aucun détenteur trouvé, recherchez un autre détenteur"
        redirect_to previous_wizard_path and return
      else
        @values = extract_data(@results)
      end
      @markers = extract_api_markers(@results)
      @form = SelectDetenteurForm.new()
    when "adressecontrole"
      @adressedetenteur = Adress.find(@controle.detenteur.adress_id)
      @markers = [{:latlng => [@adressedetenteur.lat,@adressedetenteur.long], :popup => @adressedetenteur.label}]
    when "validationnewadresse"
      res = search_adresse(params[:requete])
      if !res
        flash[:danger] = "Aucune adresse trouvée, recherchez une nouvelle adresse"
        redirect_to previous_wizard_path and return
      else
        @adresses = get_adresses_from_results(res)
        @markers = get_markers_from_results(res)
      end
    when "instruments"
    end
    render_wizard
  end
  

  def update
    @controle = Controle.find(params[:controle_id])
    case step
    when "date"
      @controle.update(valid_params(step))
    when "searchdetenteur"
      #validation du formulaire
      @form = DetenteurForm.new(params.require(:detenteur_form).permit(:requete))
      if @form.valid?
        session[:requete] = @form.requete  
      else
        render_wizard and return
      end
    when "detenteur"
      @form = SelectDetenteurForm.new(params.require(:select_detenteur_form).permit(:choix))
      if @form.valid?
        #recherche le siret sélectionné sur l'api entreprise
        et = search_siret(@form.choix)
        if Detenteur.exists?(:name => et["etablissement"]["nom_raison_sociale"])
          detenteur = Detenteur.find_by(name: et["etablissement"]["nom_raison_sociale"])
        else
          detenteur = Detenteur.create!(name: et["etablissement"]["nom_raison_sociale"], 
                          societe: et["etablissement"]["activite_principale_entreprise"],
                          siren: et["etablissement"]["siren"])
        end
        
        if !!detenteur.adress_id
          adresse = Adress.find(detenteur.adress_id)
        else
          adresse = Adress.create!(
            label:       et["etablissement"]["geo_adresse"],
            housenumber: et["etablissement"]["numero_voie"],   
            postcode:    et["etablissement"]["code_postal"],   
            citycode:    et["etablissement"]["commune"],  
            city:        et["etablissement"]["libelle_commune"],
            lat:         et["etablissement"]["latitude"], 
            long:        et["etablissement"]["longitude"], 
            context:     et["etablissement"]["libelle_region"],
            departement_id: code_postal_to_departement(et["etablissement"]["code_postal"])
            )
        end
        #et on sauvegarde les données
        @controle.detenteur = detenteur
        detenteur.adress_id = adresse.id
        detenteur.save
        adresse.save
        @controle.save
      else
        flash[:danger] = "Aucun détenteur sélectionné, recherchez à nouveau"
        redirect_to previous_wizard_path and return
      end
    when "adressecontrole"
      if !!params[:adress][:choixadressedetenteur]
        @controle.adress_id = @controle.detenteur.adress_id
        @controle.save
        jump_to(:instruments)
      else
        if !(params[:adress][:adress_id] == "")
          @controle.adress_id = params[:adress][:adress_id]
          @controle.save
          jump_to(:instruments)
        else
          skip_step(requete: params[:adress][:adress_label])
        end
      end
    when "validationnewadresse"
      adresse = create_adresse_if_new(adress_params)
      @controle.update(adress_id: adresse.id, adress_label: adresse.label)
    when "instruments"
      puts "paramètres en cours: #{valid_params(step)}"
      if @controle.update_attributes(valid_params(step))
        flash[:success] = "Controle enregistré !"
      else
        flash[:danger] = "Erreur lors de la sauvegarde du contrôle"
      end
    when "resume"
    end
    
    render_wizard @controle
  end
  
  private

  def valid_params(step)
  	permitted_attributes = case step
  	  when "date"
  	    [:date, :user_id]
  	 when "detenteur"
  	    [:date, :detenteur_id, :user_id, :categorie_id, :adress_label, :adress_id, 
  	    ]
  	  when "validationadresse"
  	    [:adress_id]

  	  when "instruments"
  	    [instruments_attributes: [:name, :detenteur_id, :categorie_id, :_destroy, 
                                  :dernierevp, :carnetmetro, :accepte, :refuse, :avertissement, :pv, :scelles, :motif,
        details: [:marque, :type, :numserie]]]
  	  end
  	params.require(:controle).permit(permitted_attributes).merge(form_step: step)
  end
  
  def adress_params
    #params.require(:adress).permit(attributes: [:housenumber, :label, :postcode, :citycode, :city, :context, :lat, :long])
    eval(params.require(:adress).permit(:attributes)[:attributes])
  end
  def controle_params
    params
    .require(:controle)
    .permit()
  end
end

class DetenteurForm
    include ActiveModel::Model
    
    attr_accessor :requete
    validates :requete, presence: {message: ": ne peut pas être vide, inscrivez le nom d'un détenteur."}
end

class SelectDetenteurForm
  include ActiveModel::Model
  attr_accessor :choix
  validates :choix, presence: {message: ": vous devez choisir un détenteur."}
end

